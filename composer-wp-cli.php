<?php
/**
 * Plugin Name:       Composer WP CLI
 * Plugin URI:        https://gitlab.com/wp-cli-packages/composer-wp-cli
 * Description:       Check installed WordPress plugins and themes for Composer compatibility.
 * Version:           0.1.0
 * Requires at least: 6.0
 * Requires PHP:      7.4
 * Tested up to:      6.4.3
 * Author:            Filip Van Reeth
 * Author URI:        https://filipvanreeth.com
 * License:           MIT
 * Update URI:        https://gitlab.com/wp-cli-packages/composer-wp-cli
 */

if ( ! defined( 'WP_CLI' ) ) {
	return;
}

if (class_exists('WP_CLI')) {
    WP_CLI::add_command('composer', FilipVanReeth\WpComposerCli\Composer::class);
}
