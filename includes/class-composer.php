<?php
namespace FilipVanReeth\WpComposerCli;

use WP_CLI_Command;
use WP_CLI;

class Composer extends WP_CLI_Command
{
    /**
     * Checks and lists all plugins for Composer compatibility.
     * 
     * @subcommand plugins
     */
    public function plugins()
    {
        $plugins = get_plugins();
        $mu_plugins = get_mu_plugins();

        $all_plugins = array();

        foreach ($plugins as $file_path => $plugin) {
            $all_plugins[$file_path] = [
                'type' => 'plugin',
                'data' => $plugin
            ];
        }

        foreach ($mu_plugins as $file_path => $mu_plugin) {
            $all_plugins[$file_path] = [
                'type' => 'mu-plugin',
                'data' => $mu_plugin
            ];
        }

        $composer_data = [];

        foreach ($all_plugins as $file_path => $plugin) {
            $plugin_type = $plugin['type'];
            $plugin_data = $plugin['data'];

            $file_path_args = explode('/', $file_path);
            $plugin_name = array_shift($file_path_args);
            $is_active = $this->is_plugin_active($file_path);

            $plugin_path = WP_PLUGIN_DIR . '/' . $plugin_name;

            $has_composer_file = $this->has_composer_json_file($plugin_path);

            $composer_installed_plugins = $this->get_composer_installed_plugins();
            $composer_installed = in_array($plugin_name, $composer_installed_plugins['default']);

            $composer_data[] = [
                'name' => $plugin_data['Name'],
                'type' => $plugin_type,
                'version' => $plugin_data['Version'],
                'active' => $is_active,
                'composer_file' => $has_composer_file,
                'composer_installed' => $composer_installed,
                'wpackagist' => $this->is_wordpress_org($plugin_name)
            ];
        }

        $this->render_table($composer_data);
        $this->get_composer_installed_plugins();
    }

    private function render_table($table_data)
    {
        foreach ($table_data as $data) {
            $table_data_array[] = [
                'Name' => $data['name'],
                'Type' => $data['type'] === 'plugin' ? 'Plugin' : 'Must-Use',
                'Version' => $data['version'],
                'Status' => $data['active'] ? 'Active' : 'Inactive',
                'Composer File' => $data['composer_file'] ? 'Yes' : 'No',
                'Composer Installed' => $data['composer_installed'] ? 'Yes' : 'No',
                'WordPress Packagist' => $data['wpackagist'] ? 'Yes' : 'No'
            ];
        }

        usort($table_data_array, function ($a, $b) {
            return strcmp($a['Name'], $b['Name']);
        });

        WP_CLI\Utils\format_items('table', $table_data_array, ['Name', 'Type', 'Version', 'Status', 'Composer File', 'Composer Installed', 'WordPress Packagist']);
    }

    private function is_plugin_active($plugin_path)
    {
        include_once(ABSPATH . 'wp-admin/includes/plugin.php');

        return is_plugin_active($plugin_path);
    }

    private function has_composer_json_file($plugin_path)
    {
        $composer_json_file = glob($plugin_path . '/composer.json');

        if (!$composer_json_file) {
            return false;
        }

        return true;
    }

    private function is_wordpress_org($plugin_name)
    {
        $response = wp_remote_get("https://api.wordpress.org/plugins/info/1.0/$plugin_name.json");

        if (is_wp_error($response) || wp_remote_retrieve_response_code($response) !== 200) {
            return false;
        }

        return true;
    }

    private function get_composer_vendor_dir()
    {
        $shell_command = shell_exec('composer config --absolute vendor-dir');

        if (!$shell_command) {
            return null;
        }

        return trim($shell_command);
    }

    private function get_composer_installed_plugins()
    {
        $composer_vendor_dir = $this->get_composer_vendor_dir();

        $installed_json = glob("$composer_vendor_dir/composer/installed.json");
        $installed_json = array_shift($installed_json);

        $installed_file = json_decode(file_get_contents($installed_json), true);

        $packages = $installed_file['packages'];

        $plugins = array_filter($packages, function ($package) {
            return strpos($package['type'], 'wordpress-plugin') !== false;
        });

        // $wpackagist_plugins = array_filter($plugins, function ($plugin) {
        //     return strpos($plugin['name'], 'wpackagist-plugin') !== false;
        // });

        // $non_wpackagist_plugins = array_filter($wpackagist_plugins, function ($plugin) {
        //     return strpos($plugin['name'], 'wpackagist-plugin') === false;
        // });

        $plugins = array_column($plugins, 'name');

        $plugins = array_map(function ($plugin) {
            $plugin_args = explode('/', $plugin);
            $plugin_name = array_pop($plugin_args);
            return $plugin_name;
        }, $plugins);

        $dev_packages = $installed_file['dev-package-names'];

        $dev_plugins = array_filter($dev_packages, function ($dev_package) {
            return strpos($dev_package, 'wpackagist-plugin') !== false;
        });

        $dev_plugins = array_map(function ($dev_package) {
            return str_replace('wpackagist-plugin/', '', $dev_package);
        }, $dev_plugins);

        $wpackagist_plugin_names = array_merge($plugins, $dev_plugins);

        return array(
            'default' => $plugins,
            'custom' => 'custom'
        );
    }
}